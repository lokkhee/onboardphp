<?php

namespace App\Http\Controllers;

use App\ShopperReview;
use Illuminate\Http\Request;

class WebController extends Controller
{

    public function list()
    {
        $reviews = ShopperReview::all();
        //dd($reviews);

        return view('web', [
            'reviews' => $reviews,
        ]);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('web');
    }
}
