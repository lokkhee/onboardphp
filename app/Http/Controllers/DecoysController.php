<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DecoysController extends Controller
{
    public function stores()
    {
        return view('decoys.stores');
    }

    public function deals()
    {
        return view('decoys.deals');
    }

    public function posts()
    {
        return view('decoys.posts');
    }
}
