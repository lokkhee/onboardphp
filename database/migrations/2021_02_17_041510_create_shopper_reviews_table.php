<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopperReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopper_reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image');
            $table->string('profile');
            $table->string('name');
            $table->string('nameid');
            $table->string('type');
            $table->string('postid');
            $table->string('title');
            $table->string('description');
            $table->integer('helpful');
            $table->integer('views');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopper_reviews');
    }
}
