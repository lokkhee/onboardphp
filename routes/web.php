<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@list');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/web', 'WebController@list')->name('web');
Route::get('/web/login', 'WebController@list');
Route::get('/web/register', 'WebController@list');

Route::get('/web/stores', 'DecoysController@stores')->name('stores');

Route::get('/web/deals', 'DecoysController@deals')->name('deals');

Route::get('/web/posts', 'DecoysController@posts')->name('posts');
