/* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
    var currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos) {
        document.getElementById("finds-footer").style.height = "60px";
    } else {
        document.getElementById("finds-footer").style.height = "0px";
    }
    prevScrollpos = currentScrollPos;
}


/* Swiper for content slide*/
const swiper_shopper_review = new Swiper('.shopper_review > .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: false,
    freeMode: true,
    slidesPerView: 1,
    spaceBetween: 15, 
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        spaceBetween: 20,
    },
    // Responsive breakpoints
    breakpoints: {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetweenSlides: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 3,
            spaceBetweenSlides: 10
        },
        // when window width is <= 640px
        1000: {
            slidesPerView: 5,
            spaceBetweenSlides: 15
        }
    }
});

const swiper_item_you_may_like = new Swiper('.item_you_may_like > .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: false,
    freeMode: true,
    slidesPerView: 1,
    spaceBetween: 15,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        spaceBetween: 20,
    },
    // Responsive breakpoints
    breakpoints: {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetweenSlides: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 3,
            spaceBetweenSlides: 10
        },
        // when window width is <= 640px
        1000: {
            slidesPerView: 5,
            spaceBetweenSlides: 15
        }
    }
});

const swiper_featured_stores = new Swiper('.featured_stores > .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: false,
    freeMode: true,
    slidesPerView: 1,
    spaceBetween: 15,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        spaceBetween: 20,
    },
    // Responsive breakpoints
    breakpoints: {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetweenSlides: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 3,
            spaceBetweenSlides: 10
        },
        // when window width is <= 640px
        1000: {
            slidesPerView: 5,
            spaceBetweenSlides: 15
        }
    }
});

const swiper_my_3 = new Swiper('.my-3 > .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: false,
    freeMode: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },

});

const swiper_event_highlight_promo = new Swiper('.event_highlight_promo > .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: false,
    freeMode: true,
    slidesPerView: 2,
    spaceBetween: 30,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        spaceBetween: 20,
    },
    // Responsive breakpoints
    breakpoints: {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetweenSlides: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 2,
            spaceBetweenSlides: 30
        },
    }

});
