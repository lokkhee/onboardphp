<style>
    #liveChat>* {
        font-size: .85rem;
    }

    /* Button used to open the contact form - fixed at the bottom of the page */
    .open-button {
        background-color: #f9482b;
        color: white;
        padding: 16px 20px;
        border: none;
        cursor: pointer;
        opacity: 1;
        position: fixed;
        bottom: 20px;
        right: 20px;
        width: 60px;
        height: 60px;
        z-index: 20;
    }

    /* The popup form - hidden by default */
    .form-popup {
        display: none;
        position: fixed;
        bottom: 0;
        right: 15px;
        border: 3px solid #f1f1f1;
        z-index: 21;
        margin-bottom: 60px;
    }

    /* Add styles to the form container */
    .form-container {
        max-width: 300px;
        padding: 10px;
        background-color: white;
    }

    /* Full-width input fields */
    .form-container input {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        border: none;
        background: #f1f1f1;

    }

    /* Set a style for the submit/login button */
    .form-container .cancel {
        background-color: #f9482b;
        color: white;
        border: none;
        cursor: pointer;
        opacity: 1;
        position: fixed;
        bottom: 20px;
        right: 20px;
        width: 60px;
        height: 60px;
        border-radius: 50%;
    }

    /* Add some hover effects to buttons */
    .form-container .cancel:hover,
    .open-button:hover {
        opacity: 1;
    }
</style>


<button class="open-button rounded-circle" onclick="openForm()">O</button>

<div class="form-popup" id="liveChat">
    <form action="" class="form-container">
        <h1>Send message</h1>
        <p>Please fill out the form below and we will get back to you as soon as possible.</p>

        <input type="text" autocomplete="off" placeholder="Enter Name" name="name" required>
        <input type="text" autocomplete="off" placeholder="Enter Email" name="email" required>

        <button type="button" class="btn cancel" onclick="closeForm()"> X </button>
    </form>
</div>


<script>
    function openForm() {
        document.getElementById("liveChat").style.display = "block";
    }

    function closeForm() {
        document.getElementById("liveChat").style.display = "none";
    }
</script>