<div class="header mb-sm-0 pb-sm-0">
    <nav role="navigation" class="navbar navbar-site navbar-light navbar-expand-md p-0">
        <div class="border-orange-2 w-100">
            <div class="container">
                <button id="navbar_btn" type="button" data-target=".navbar-collapse" data-toggle="collapse" aria-expanded="false" class="navbar-toggler pull-left text-dark border-0 bg-tansparent mt-1">
                    <i class="fas fa-bars"></i>
                </button>

                <a class="navbar-brand d-flex" href="{{ route('web') }}">
                    <div><img src="/images/logo.png" style="height: 30px;"></div>
                </a>

                <!-- Left Side Of Navbar -->
                <ul class="nav m-auto py-3">
                    <li class="nav-item mr-4 d-none d-md-block">
                        <a href="{{ route('stores') }}" class="nav-link text-dark">
                            <div class="d-flex align-items-center">
                                <img src="/images/icon/cashback_black.svg" title="Cashback" class="svg-invert mr-1">
                                <span class="d-sm-none d-lg-inline"> Cashback </span>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item mr-4 d-none d-md-block">
                        <a href="{{ route('deals') }}" class="nav-link text-dark">
                            <div class="d-flex align-items-center">
                                <img src="/images/icon/deal_black.svg" title="Deals" class="svg-invert mr-1">
                                <span class="d-md-none d-lg-inline"> Deals </span>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item mr-4 d-none d-md-block">
                        <a href="{{ route('posts') }}" class="nav-link text-dark">
                            <div class="d-flex align-items-center">
                                <img src="/images/icon/review-black.svg" title="Reviews" class="svg-invert mr-1">
                                <span class="d-md-none d-lg-inline"> Reviews </span>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item mr-4 mt-1 d-none d-md-block dropdown">
                        <a id="dropdown_more_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link text-dark">
                            <div class="d-flex align-items-center"><i class="fa fa-ellipsis-h" style="cursor: pointer"></i></div>
                        </a>
                        <div id="dropdown_more_menu_item" aria-labelledby="dropdownMenuButton" class="dropdown-menu left-auto mt-0">
                            <i class="fas fa-caret-up custom-arrow-up ml-1"></i>
                            <a href="/web/product-vouchers/" class="dropdown-item py-2 bg-white text-dark"><img src="/images/icon/voucher-black.svg" class="mr-1"> Vouchers</a>
                            <a href="/web/rewards/" class="dropdown-item py-2 bg-white text-dark"><img src="/images/icon/reward_black.svg" class="mr-1"> Rewards</a>
                            <a href="/web/wishes/" class="dropdown-item py-2 bg-white text-dark"><img src="/images/icon/wish_black.svg" class="mr-1"> Wish Tree</a>
                            <!---->
                        </div>
                    </li>
                    <!---->
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav nav float-right">
                    <li class="nav-item">
                        <a href="https://octaplus.io/web/faq" class="nav-link text-dark" title="Help">
                            <i class="far fa-question-circle fa-lg"></i>
                        </a>
                    </li>
                    <!-- Authentication Links -->
                    @guest
                    <li class="nav-item">
                        <a class="nav-link" style="cursor: pointer" data-toggle="modal" data-target="#loginModal"> Login / Register </a>
                    </li>
                    @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                </ul>
            </div>
        </div>
        <div class="col-sm-8 d-block d-md-none shadow-sm p-0">
            <div class="navbar-collapse pt-0 bg-white collapse">
                <ul class="nav navbar-nav navbar-left m-auto border-bottom">
                    <li class="nav-item py-2"><a href="/web/stores" class="nav-link font-15">
                            <div class="d-flex align-items-center"><img src="/images/icon/cashback_black.svg" class="mr-1"> Cashback
                            </div>
                        </a></li>
                    <li class="nav-item py-2"><a href="/web/deals" class="nav-link font-15">
                            <div class="d-flex align-items-center"><img src="/images/icon/deal_black.svg" class="mr-1"> Deals
                            </div>
                        </a></li>
                    <li class="nav-item py-2"><a href="/web/posts/" class="nav-link font-15">
                            <div class="d-flex align-items-center"><img src="/images/icon/review-black.svg" class="mr-1"> Reviews
                            </div>
                        </a></li>
                    <li class="nav-item py-2"><a href="/web/rewards/" class="nav-link font-15">
                            <div class="d-flex align-items-center"><img src="/images/icon/reward_black.svg" class="mr-1"> Rewards
                            </div>
                        </a></li>
                    <li class="nav-item py-2"><a href="/web/wishes/" class="nav-link font-15">
                            <div class="d-flex align-items-center"><img src="/images/icon/wish_black.svg" class="mr-1"> Wish Tree
                            </div>
                        </a></li>
                    <li class="nav-item py-2"><a href="/web/product-vouchers/" class="nav-link font-15">
                            <div class="d-flex align-items-center"><img src="/images/icon/voucher-black.svg" class="mr-1"> Vouchers
                            </div>
                        </a></li>
                    <li class="nav-item py-2"><a data-target=".navbar-collapse" data-toggle="collapse" class="nav-link font-15" aria-expanded="false">
                            <div class="d-flex align-items-center"><img src="/images/icon/refer.svg" class="mr-1"> Refer &amp; Earn
                            </div>
                        </a></li>
                </ul>
                <ul class="nav navbar-nav navbar-left m-auto">
                    <li class="nav-item mt-1 py-1"><a href="/web/cashback/how-it-works" class="nav-link font-15 "> How OctaPlus Work? </a></li>
                    <li class="nav-item py-1"><a href="/web/faq" class="nav-link font-15 "> Need Help? </a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>