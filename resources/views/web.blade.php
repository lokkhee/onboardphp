@extends('layouts.app')

@section('content')
<!-- highlight slide -->
<div class="container">
    <div class="my-3">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><a href="https://octaplus.io/web/stores" target="_blank"><img src="https://file.octaplus.io/public/banner/tF0eqCWDrv356eihAkJw3hBMdMaTNuRVkW4ARxMl.jpeg?1613525231" alt="Feb Fabulous Special Upsized Campaign Weekly Highlight" class="m-auto mh-250 w-100"></a></div>
                <div class="swiper-slide"><a href="https://octaplus.io/web/stores" target="_blank"><img src="https://file.octaplus.io/public/banner/hqtdDrDRQlkH55eqHzPr3vE8NSdfcu3wteRx9gSt.jpeg?1612921795" alt="Feb Fabulous Special Upsized Campaign" class="m-auto mh-250 w-100"></a></div>
                <div class="swiper-slide"><a href="https://octaplus.io/web/faq#faq-5" target="_blank"><img src="https://file.octaplus.io/public/banner/HqM3mpKY2VzFzJ5yf1qyqpZfViRw7KcftYSwxjfm.jpeg?1592899966" alt="Convert your cashback into reward points to redeem more attractive prizes!" class="m-auto mh-250 w-100"></a></div>
                <div class="swiper-slide"><a href="https://octaplus.io/web/user/wishes/wall#top_nav" target="_blank"><img src="https://file.octaplus.io/public/banner/qUvVVpTjW74jzduvnLNFz9mByixBHshzlAg3iiOl.jpeg?1606284595" alt="Check in daily for greater rewards!!!" class="m-auto mh-250 w-100"></a></div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
    <!-- content -->
    <div class="container">
        <section class="event-featured bg-white">
            <div class="container p-0 mt-4">
                <div class="row">
                    <div class="col-md-6 text-center my-auto">
                        <img src="https://octaplus.io/img/web/search-bg.png" class="img-fluid d-none d-md-block">
                    </div>
                    <div class="col-md-6 my-md-auto">
                        <div>
                            <p class="h1 d-none d-md-block"> Find Yours. The best price will talk.</p>
                            <p class="h4 font-weight-bold d-sm-block d-md-none"> Find Yours. The best price will talk. </p>
                            <p class="text-muted mb-0">
                                Your new shopping assistant specializing in searching for the best price across multiple marketplaces! Psst, you may be able to earn some cashbacks from Octaplus merchants too!
                            </p>
                            <a href="https://octaplus.io/web/find-yours/product-price-compare" class="text-orange" style="text-decoration:none;">
                                Learn More &gt;&gt;
                            </a>
                            <form class="pt-3">
                                <div class="row mx-0">
                                    <div class="col px-0">
                                        <div id="center_recent_search" class="input-group-append p-0">
                                            <input autocomplete="off" type="text" placeholder="Search for deals, etc" required="required" class="form-control">
                                            <!---->
                                        </div>
                                        <!---->
                                    </div>
                                    <div class="col-auto px-0">
                                        <div class="input-group-append p-0">
                                            <button type="submit" class="btn btn-block bg-orange z-index-0 text-white" style="height: 38px;">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </section>


        <section class="event-featured bg-white">
            <div class="container p-0 mt-4">
                <div class="row">
                    <!-- Cashback Stores -->
                    <div class="col-md title-box section-header pb-3">
                        <div class="row align-items-center mx-0">
                            <div class="col text-left mb-2"><span class="title text-capitalize mb-0">
                                    Cashback Stores
                                </span></div>
                            <div class="col-auto mb-2"><a href="{{ route('stores') }}" class="text-orange font-weight-bold" style="text-decoration:none;">
                                    More Stores
                                </a></div>
                            <div class="col-sm-12">
                                <div class="row border rounded-lg mx-0 " id="cashback_stores" style="height:248px;">
                                    <div class="col-6 col-sm-4 text-center p-2"><a href="https://octaplus.io/store/zalora-my/5dc0066d69f12b542c071992/detail">
                                            <div class="d-flex align-items-center height-75 px-2 overflow-hidden"><img class="img-responsive w-100 mx-auto" data-src="https://file.octaplus.io/public/store/0daVOmY4vXTsxBo3neZBBRH3zUSKYGAxVwk9QErj.jpeg" src="https://file.octaplus.io/public/store/0daVOmY4vXTsxBo3neZBBRH3zUSKYGAxVwk9QErj.jpeg" lazy="loaded"></div>
                                            <p class="text-muted text-truncate mt-1 mb-2"><span>Up to 2% Cashback</span></p>
                                        </a></div>
                                    <div class="col-6 col-sm-4 text-center p-2"><a href="https://octaplus.io/store/nike-my/5d504ed53aae55288e11cb33/detail">
                                            <div class="d-flex align-items-center height-75 px-2 overflow-hidden"><img class="img-responsive w-100 mx-auto" data-src="https://file.octaplus.io/public/store/1q1HnBKVUnptvbNuXspRFdEnpxcQ02GGi4mxpsHC.jpeg" src="https://file.octaplus.io/public/store/1q1HnBKVUnptvbNuXspRFdEnpxcQ02GGi4mxpsHC.jpeg" lazy="loaded"></div>
                                            <p class="text-muted text-truncate mt-1 mb-2"><span>4% Upsized</span></p>
                                        </a></div>
                                    <div class="col-6 col-sm-4 text-center p-2"><a href="https://octaplus.io/store/motherhood-my/5d5f584869f12b4a0a244cf2/detail">
                                            <div class="d-flex align-items-center height-75 px-2 overflow-hidden"><img class="img-responsive w-100 mx-auto" data-src="https://file.octaplus.io/public/store/P27CK8g0fKpy7k1Z6D2vh5fUY5OTIrX6UWsL4RN2.jpeg" src="https://file.octaplus.io/public/store/P27CK8g0fKpy7k1Z6D2vh5fUY5OTIrX6UWsL4RN2.jpeg" lazy="loaded"></div>
                                            <p class="text-muted text-truncate mt-1 mb-2"><span>6% Upsized</span></p>
                                        </a></div>
                                    <div class="col-6 col-sm-4 text-center p-2"><a href="https://octaplus.io/store/cotton-on-my/5d8c8d5969f12b2d8e656092/detail">
                                            <div class="d-flex align-items-center height-75 px-2 overflow-hidden"><img class="img-responsive w-100 mx-auto" data-src="https://file.octaplus.io/public/store/oupGaOjApNteGM2xPXusEc6KIM35ckP9B9oeZqwU.jpeg" src="https://file.octaplus.io/public/store/oupGaOjApNteGM2xPXusEc6KIM35ckP9B9oeZqwU.jpeg" lazy="loaded"></div>
                                            <p class="text-muted text-truncate mt-1 mb-2"><span>3% Upsized</span></p>
                                        </a></div>
                                    <div class="col-6 col-sm-4 text-center p-2"><a href="https://octaplus.io/store/althea/5e564e1169f12b7966020f32/detail">
                                            <div class="d-flex align-items-center height-75 px-2 overflow-hidden"><img class="img-responsive w-100 mx-auto" data-src="https://file.octaplus.io/public/store/cdFuXoUACsMr6U9u4mJHbBNBGSSUX8SzZroPq1Bc.jpeg" src="https://file.octaplus.io/public/store/cdFuXoUACsMr6U9u4mJHbBNBGSSUX8SzZroPq1Bc.jpeg" lazy="loaded"></div>
                                            <p class="text-muted text-truncate mt-1 mb-2"><span>5% Upsized</span></p>
                                        </a></div>
                                    <div class="col-6 col-sm-4 text-center p-2"><a href="https://octaplus.io/store/watsons-my/5cea0a2b69f12b4d8f34e84a/detail">
                                            <div class="d-flex align-items-center height-75 px-2 overflow-hidden"><img class="img-responsive w-100 mx-auto" data-src="https://file.octaplus.io/public/store/GrJdW4kMGVecHjSTZUdEfo8uwrb3Rpoc3ZNG0wrF.jpeg" src="https://file.octaplus.io/public/store/GrJdW4kMGVecHjSTZUdEfo8uwrb3Rpoc3ZNG0wrF.jpeg" lazy="loaded"></div>
                                            <p class="text-muted text-truncate mt-1 mb-2"><span>2% Upsized</span></p>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Daily Check In -->
                    <div class="col-md title-box section-header pb-3">
                        <div class="row align-items-center mx-0">
                            <div class="col text-left mb-2"><span class="title text-capitalize mb-0">
                                    Daily Check-In
                                </span></div>
                            <div class="col-sm-12">
                                <div class="row border rounded-lg mx-0 h-248" style="height:248px;">
                                    <div class="col-auto my-auto"><img src="/images/web/reward/reward-point.png" class="img-responsive w-100"></div>
                                    <div class="col pr-5 my-auto">
                                        <p> Earn up to 300 points while you check-in 7 consecutive days </p> <a class="btn bg-orange text-white rounded-lg px-auto px-sm-5"> Check-In Now </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <!-- Shopper Review -->
        <section class="container p-0 mt-4">
            <div class="section-header pb-3">
                <div class="row align-items-center">
                    <div class="col px-1 custom-border-line-light px-0 text-left">
                        <span class="mb-0 title text-capitalize custom-border-title mobile-font-4vw"> Shopper Review </span>
                    </div>
                    <div class="col-auto">
                        <a href="{{ route('posts') }}" class="btn bg-orange text-white rounded-lg px-4">View All</a>
                    </div>
                </div>
            </div>
            <div class="pb-2 has-white-bg shopper_review">
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides using database-->
                        @if (count($reviews) > 0)
                        @foreach($reviews as $review)
                        <div class="swiper-slide">
                            <div class="box-custom-post border rounded-lg hover-highlight">
                                <div class="row mx-0">
                                    <div class="col px-0">
                                        <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer"><img alt="{{ $review->title }}" data-holder-rendered="true" class="post-img" src="{{ $review->image }}" lazy="loaded"></div>
                                        <div class="custom-dropdown-post">
                                            <div class="dropdown"><a id="dropdownBtn_{{ $review->postid }}_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-outline-secondary border-0 rounded-lg px-2"><em class="fa fa-ellipsis-v"></em></a>
                                                <div id="dropdown_{{ $review->postid }}_menu" class="dropdown-menu dropdown-menu-post mt-0"><i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> <a is_follow="0" class="dropdown-item py-1 w-auto user_{{ $review->nameid }}">
                                                        <div class="d-flex align-items-center"><i class="fas fa-check mr-1"></i> Follow
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-2 mx-0">
                                    <div class="col px-2"><a href="https://octaplus.io/user/{{ $review->name }}/{{ $review->nameid }}/detail" target="_blank" title="{{ $review->name }}" class="text-dark">
                                            <div class="d-flex align-items-center"><img alt="{{ $review->name }}" class="post-user-icon rounded-circle mr-1" src="{{ $review->profile }}" lazy="loaded"> <span class="font-weight-bold text-truncate">{{ $review->name }}</span></div>
                                        </a></div>
                                </div>
                                <div class="row mx-0">
                                    <div class="col">
                                        <div class="d-flex align-items-center">
                                            <p class="text-orange mt-2 mb-1 text-capitalize"> {{ $review->type }} </p>
                                        </div>
                                        <div class="h-45"><a href="https://octaplus.io/post/{{ $review->title }}/{{ $review->postid }}/detail" target="_blank" title="{{ $review->title }}" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark">{{ $review->title }}</a></div>
                                        <div class="h-36"><a href="https://octaplus.io/post/{{ $review->title }}/{{ $review->postid }}/detail" target="_blank" title="{{ $review->description }}" class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted"> {{ $review->description }} </a></div>
                                    </div>
                                </div>
                                <div class="row my-2 mx-0">
                                    <div class="col-auto pr-0"><a href="https://octaplus.io/post/{{ $review->title }}/{{ $review->postid }}/detail#blogs-helpful" target="_blank" class="font-size-12 text-black-50">
                                            <div class="d-flex align-items-center"><img src="/images/web/post/default_help.svg" class="mh-20 mr-1"> <span title="{{ number_format($review->helpful,0)}}" class="text-truncate"> {{ number_format($review->helpful,0)}} </span></div>
                                        </a></div>
                                    <div class="col pr-0">
                                        <div class="d-flex align-items-center"><img src="/images/web/post/view.svg" class="mr-1"> <span title="{{ number_format($review->views,0) }}" class="font-size-12 text-black-50 text-truncate"> {{ number_format($review->views,0) }} </span></div>
                                    </div>
                                    <div class="col-auto"><a class="btn-link"><i class="fa-bookmark font-18 far text-black-50"></i></a></div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <!-- Slides -->
                        <div class="swiper-slide p-5">Slide 1</div>
                        <div class="swiper-slide p-5">Slide 2</div>
                        <div class="swiper-slide p-5">Slide 3</div>
                        <div class="swiper-slide p-5">Slide 4</div>
                        <div class="swiper-slide p-5">Slide 5</div>
                        <div class="swiper-slide p-5">Slide 6</div>
                        <div class="swiper-slide p-5">Slide 7</div>
                        <div class="swiper-slide p-5">Slide 8</div>
                        <div class="swiper-slide p-5">Slide 9</div>
                        @endif
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination col-12 text-center p-5"></div>
                </div>
            </div>
        </section>

        <!-- Item You May Like -->
        <section class="container p-0 mt-4">
            <div class="section-header pb-3">
                <div class="row align-items-center">
                    <div class="col px-1 custom-border-line-light px-0 text-left">
                        <span class="mb-0 title text-capitalize custom-border-title mobile-font-4vw"> Item You May Like </span>
                    </div>
                    <div class="col-auto">
                        <a href="{{ route('deals') }}" class="btn bg-orange text-white rounded-lg px-4">View All</a>
                    </div>
                </div>
            </div>
            <div class="pb-2 has-white-bg item_you_may_like">
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        @if (count($reviews) > 0)
                        @foreach($reviews as $review)
                        <!-- Slides -->
                        <div class="swiper-slide p5">
                            <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                                <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer">
                                    <img data-holder-rendered="true" class="deal-img" src="https://image.octaplus.io/deal/60129672824656a3a30220ea/image.jpg?id=eyJpdiI6IkZFVms0aHJGVVVndkFnSE9vRk1Temc9PSIsInZhbHVlIjoicmRiZFZWUDBXT2F5YnFTRU5BZVZoZjkwanpOYWwxc0pZbU8zUVBNY1ZhOGtydVl3am5lY1BidmhUMVpcL2ExdFpQQlFvVkJsNzFGMW51MkFzQ3FkZnVNb2pcL0s1Y3JrM3ZZQWF2OTE5U1dzWT0iLCJtYWMiOiI2ZWY0NDBkZGVjMTJkOGM5YWM1OGU4MDM4OTI3YzE1NWQ0MzIyZWVlNTI1N2Q4MmU3NDNkYzZmNzg5MTAyMWNiIn0%3D" lazy="loaded">
                                </div>
                                <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                                    <div class="row mx-0">
                                        <div class="col my-auto p-0 border-right"><a deal_id="60129672824656a3a30220ea" class="btn-link h5 text-black-50"><i class="fa-sm far fa-heart"></i></a></div>
                                        <div class="col my-auto p-0"><a title="Cashback available for this merchant" class="btn-cashback text-white rounded-circle bg-orange border-0"><i class="fa-sm fas fa-dollar-sign"></i></a></div>
                                    </div>
                                </div>
                                <div class="row mx-0">
                                    <div class="col px-1">
                                        <!---->
                                        <div class="h-45 mt-20"><a href="https://octaplus.io/deal/instant-prepaid-topup-direct-topup/60129672824656a3a30220ea/detail" target="_blank" title="INSTANT PREPAID TOPUP (DIRECT TOPUP)" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark">INSTANT PREPAID TOPUP (DIRECT TOPUP)</a></div>
                                        <p class="font-size-12 mb-0 text-muted"> Shopee (MY) </p>
                                        <div class="mb-1">
                                            <div class="d-flex align-items-center">
                                                <div data-v-34cbeed1="" class="mt--2 vue-star-rating">
                                                    <div data-v-34cbeed1="" class="vue-star-rating"><span data-v-34cbeed1="" class="vue-star-rating-star" style="margin-right: 0px;"><svg data-v-21f5376e="" data-v-34cbeed1="" height="12" width="12" viewBox="0 0 12.000000000000002 12.000000000000002" class="vue-star-rating-star" step="100">
                                                                <linearGradient data-v-21f5376e="" id="qlcfs1k" x1="0" x2="100%" y1="0" y2="0">
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#ffd055"></stop>
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#d8d8d8"></stop>
                                                                </linearGradient>
                                                                <filter data-v-21f5376e="" id="3u9gzv" height="130%" width="130%" filterUnits="userSpaceOnUse">
                                                                    <feGaussianBlur data-v-21f5376e="" stdDeviation="0" result="coloredBlur"></feGaussianBlur>
                                                                    <feMerge data-v-21f5376e="">
                                                                        <feMergeNode data-v-21f5376e="" in="coloredBlur"></feMergeNode>
                                                                        <feMergeNode data-v-21f5376e="" in="SourceGraphic"></feMergeNode>
                                                                    </feMerge>
                                                                </filter>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#qlcfs1k)" stroke="#fff" filter="url(#3u9gzv)"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#qlcfs1k)" stroke="#999" stroke-width="0" stroke-linejoin="miter"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#qlcfs1k)"></polygon>
                                                            </svg></span><span data-v-34cbeed1="" class="vue-star-rating-star" style="margin-right: 0px;"><svg data-v-21f5376e="" data-v-34cbeed1="" height="12" width="12" viewBox="0 0 12.000000000000002 12.000000000000002" class="vue-star-rating-star" step="100">
                                                                <linearGradient data-v-21f5376e="" id="v016q8" x1="0" x2="100%" y1="0" y2="0">
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#ffd055"></stop>
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#d8d8d8"></stop>
                                                                </linearGradient>
                                                                <filter data-v-21f5376e="" id="o0vx1" height="130%" width="130%" filterUnits="userSpaceOnUse">
                                                                    <feGaussianBlur data-v-21f5376e="" stdDeviation="0" result="coloredBlur"></feGaussianBlur>
                                                                    <feMerge data-v-21f5376e="">
                                                                        <feMergeNode data-v-21f5376e="" in="coloredBlur"></feMergeNode>
                                                                        <feMergeNode data-v-21f5376e="" in="SourceGraphic"></feMergeNode>
                                                                    </feMerge>
                                                                </filter>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#v016q8)" stroke="#fff" filter="url(#o0vx1)"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#v016q8)" stroke="#999" stroke-width="0" stroke-linejoin="miter"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#v016q8)"></polygon>
                                                            </svg></span><span data-v-34cbeed1="" class="vue-star-rating-star" style="margin-right: 0px;"><svg data-v-21f5376e="" data-v-34cbeed1="" height="12" width="12" viewBox="0 0 12.000000000000002 12.000000000000002" class="vue-star-rating-star" step="100">
                                                                <linearGradient data-v-21f5376e="" id="6464xpi" x1="0" x2="100%" y1="0" y2="0">
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#ffd055"></stop>
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#d8d8d8"></stop>
                                                                </linearGradient>
                                                                <filter data-v-21f5376e="" id="vhmtvi" height="130%" width="130%" filterUnits="userSpaceOnUse">
                                                                    <feGaussianBlur data-v-21f5376e="" stdDeviation="0" result="coloredBlur"></feGaussianBlur>
                                                                    <feMerge data-v-21f5376e="">
                                                                        <feMergeNode data-v-21f5376e="" in="coloredBlur"></feMergeNode>
                                                                        <feMergeNode data-v-21f5376e="" in="SourceGraphic"></feMergeNode>
                                                                    </feMerge>
                                                                </filter>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#6464xpi)" stroke="#fff" filter="url(#vhmtvi)"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#6464xpi)" stroke="#999" stroke-width="0" stroke-linejoin="miter"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#6464xpi)"></polygon>
                                                            </svg></span><span data-v-34cbeed1="" class="vue-star-rating-star" style="margin-right: 0px;"><svg data-v-21f5376e="" data-v-34cbeed1="" height="12" width="12" viewBox="0 0 12.000000000000002 12.000000000000002" class="vue-star-rating-star" step="100">
                                                                <linearGradient data-v-21f5376e="" id="tvx6h" x1="0" x2="100%" y1="0" y2="0">
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#ffd055"></stop>
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#d8d8d8"></stop>
                                                                </linearGradient>
                                                                <filter data-v-21f5376e="" id="p9my6" height="130%" width="130%" filterUnits="userSpaceOnUse">
                                                                    <feGaussianBlur data-v-21f5376e="" stdDeviation="0" result="coloredBlur"></feGaussianBlur>
                                                                    <feMerge data-v-21f5376e="">
                                                                        <feMergeNode data-v-21f5376e="" in="coloredBlur"></feMergeNode>
                                                                        <feMergeNode data-v-21f5376e="" in="SourceGraphic"></feMergeNode>
                                                                    </feMerge>
                                                                </filter>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#tvx6h)" stroke="#fff" filter="url(#p9my6)"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#tvx6h)" stroke="#999" stroke-width="0" stroke-linejoin="miter"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#tvx6h)"></polygon>
                                                            </svg></span><span data-v-34cbeed1="" class="vue-star-rating-star" style="margin-right: 0px;"><svg data-v-21f5376e="" data-v-34cbeed1="" height="12" width="12" viewBox="0 0 12.000000000000002 12.000000000000002" class="vue-star-rating-star" step="100">
                                                                <linearGradient data-v-21f5376e="" id="urh6r" x1="0" x2="100%" y1="0" y2="0">
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#ffd055"></stop>
                                                                    <stop data-v-21f5376e="" offset="100%" stop-color="#d8d8d8"></stop>
                                                                </linearGradient>
                                                                <filter data-v-21f5376e="" id="b6wuac" height="130%" width="130%" filterUnits="userSpaceOnUse">
                                                                    <feGaussianBlur data-v-21f5376e="" stdDeviation="0" result="coloredBlur"></feGaussianBlur>
                                                                    <feMerge data-v-21f5376e="">
                                                                        <feMergeNode data-v-21f5376e="" in="coloredBlur"></feMergeNode>
                                                                        <feMergeNode data-v-21f5376e="" in="SourceGraphic"></feMergeNode>
                                                                    </feMerge>
                                                                </filter>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#urh6r)" stroke="#fff" filter="url(#b6wuac)"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#urh6r)" stroke="#999" stroke-width="0" stroke-linejoin="miter"></polygon>
                                                                <polygon data-v-21f5376e="" points="5.454545454545455,0.6060606060606061,1.8181818181818181,12.000000000000002,10.90909090909091,4.7272727272727275,0,4.7272727272727275,9.090909090909092,12.000000000000002" fill="url(#urh6r)"></polygon>
                                                            </svg></span>
                                                        <!---->
                                                    </div>
                                                </div>
                                                <p class="font-size-12 ml-1 mb-0 text-grey"> (26 sold) </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mx-0">
                                    <div class="col px-1 h-45">
                                        <p title="MYR5.00" class="font-14 mb-0 font-weight-bold text-orange text-truncate"> MYR5.00 </p>
                                        <p title=" -" class="font-size-12 mb-0 text-black-50 text-truncate">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="swiper-slide p-5">Slide 1</div>
                        <div class="swiper-slide p-5">Slide 2</div>
                        <div class="swiper-slide p-5">Slide 3</div>
                        <div class="swiper-slide p-5">Slide 4</div>
                        <div class="swiper-slide p-5">Slide 5</div>
                        <div class="swiper-slide p-5">Slide 6</div>
                        <div class="swiper-slide p-5">Slide 7</div>
                        <div class="swiper-slide p-5">Slide 8</div>
                        <div class="swiper-slide p-5">Slide 9</div>
                        @endif
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination col-12 text-center p-5"></div>
                </div>
            </div>
        </section>

        <!-- Featued Store -->
        <section class="container p-0 mt-4">
            <div class="section-header pb-3">
                <div class="row align-items-center">
                    <div class="col px-1 custom-border-line-light px-0 text-left">
                        <span class="mb-0 title text-capitalize custom-border-title mobile-font-4vw"> Featured Stores </span>
                    </div>
                    <div class="col-auto">
                        <a href="{{ route('stores') }}" class="btn bg-orange text-white rounded-lg px-4">View All</a>
                    </div>
                </div>
            </div>
            <div class="pb-2 has-white-bg featured_stores">
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">

                        @if (count($reviews) > 0)
                        @foreach($reviews as $review)
                        <div class="swiper-slide">
                            <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                <div class="box-custom-store d-flex align-items-center justify-content-center px-3"><a href="https://octaplus.io/store/zalora-my/5dc0066d69f12b542c071992/detail"><img src="/images/icon/popular-store.svg" class="popular-store-badge h-26"> <img src="/images/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/0daVOmY4vXTsxBo3neZBBRH3zUSKYGAxVwk9QErj.jpeg" src="https://file.octaplus.io/public/store/0daVOmY4vXTsxBo3neZBBRH3zUSKYGAxVwk9QErj.jpeg" lazy="loaded"></a></div>
                                <p class="text-muted text-truncate mt-4"><a href="https://octaplus.io/store/zalora-my/5dc0066d69f12b542c071992/detail" title="Zalora (MY)">
                                        <p class="mb-0"> Zalora (MY) </p>
                                    </a></p>
                                <p class="text-truncate text-orange">Up to 2% Cashback</p>
                            </div>
                        </div>
                        <!-- Slides -->
                        @endforeach
                        @else
                        <div class="swiper-slide p-5">Slide 1</div>
                        <div class="swiper-slide p-5">Slide 2</div>
                        <div class="swiper-slide p-5">Slide 3</div>
                        <div class="swiper-slide p-5">Slide 4</div>
                        <div class="swiper-slide p-5">Slide 5</div>
                        <div class="swiper-slide p-5">Slide 6</div>
                        <div class="swiper-slide p-5">Slide 7</div>
                        <div class="swiper-slide p-5">Slide 8</div>
                        <div class="swiper-slide p-5">Slide 9</div>
                        @endif
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination col-12 text-center p-5"></div>
                </div>
            </div>
        </section>

        <!-- Event Highlight -->
        <section class="container p-0 mt-4">
            <div class="section-header pb-3">
                <div class="row align-items-center">
                    <div class="col px-1 custom-border-line-light px-0 text-left">
                        <span class="mb-0 title text-capitalize custom-border-title mobile-font-4vw"> Event Highlight (Promo) </span>
                    </div>
                    <div class="col-auto">
                        <a href="/web/events/" class="btn bg-orange text-white rounded-lg px-4">View All</a>
                    </div>
                </div>
            </div>
            <div class="pb-2 has-white-bg event_highlight_promo">
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @if (count($reviews) > 0)
                        @foreach($reviews as $review)
                        <div class="swiper-slide" style="width:540px">
                            <div class="row rounded-lg box-custom7 border hover-highlight hover-cursor mx-0">
                                <div class="col-12 bg-light px-0 events_mobile_img d-block d-sm-none">
                                    <!----> <a href="https://octaplus.io/event/sports-direct-slazenger-footwear-from-rm32/602e041f26d41035332d82c2/detail"><img class="img-responsive m-auto" data-src="https://file.octaplus.io/public/event/JZLRxI8Syaaof2nwJFjvIHjCQclixwf9BiHjyjlx.jpeg" src="/img/loading.gif" lazy="loading"></a>
                                </div>
                                <div class="col-sm-6 px-0 border-top-left-radius-0_3">
                                    <div class="bg-orange py-1">
                                        <p class="text-center text-white m-auto"><span>Ending in 21 Feb 2021 11:59pm</span></p>
                                    </div>
                                    <div class="p-3"><a href="https://octaplus.io/event/sports-direct-slazenger-footwear-from-rm32/602e041f26d41035332d82c2/detail" title="Sports Direct | SLAZENGER FOOTWEAR | FROM RM32" class="h6 text-dark d-block text-truncate">
                                            Sports Direct | SLAZENGER FOOTWEAR | FROM RM32
                                        </a>
                                        <p class="font-weight-bold mt-2 mb-1"><i class="far fa-calendar-alt mr-2"></i> Event Date
                                        </p>
                                        <p class="mb-1 text-truncate"> Start: 18 Feb 2021 12:00am</p>
                                        <p class="mb-1 text-truncate"> Ends: 21 Feb 2021 11:59pm</p>
                                    </div>
                                </div>
                                <div class="col-sm-6 bg-light px-0 border-top-right-radius-0_3 d-none d-sm-block bg-image" style="background-image: url(&quot;https://file.octaplus.io/public/event/JZLRxI8Syaaof2nwJFjvIHjCQclixwf9BiHjyjlx.jpeg&quot;);">
                                    <!----> <a href="https://octaplus.io/event/sports-direct-slazenger-footwear-from-rm32/602e041f26d41035332d82c2/detail" class="w-100 h-100 d-block"></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="swiper-slide" style="width:540px">Slide 1</div>
                        <div class="swiper-slide" style="width:540px">Slide 2</div>
                        <div class="swiper-slide" style="width:540px">Slide 3</div>
                        <div class="swiper-slide" style="width:540px">Slide 4</div>
                        @endif
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination col-12 text-center p-5"></div>
                </div>
            </div>
        </section>


        <section class="event-featured bg-white mb-3">
            <div class="card">
                <div class="card-body text-justify">
                    <div>
                        <p class="h5">
                            What is Octaplus
                        </p>

                        <p>
                            Octaplus is a shopping guide app that comes with loads of benefits for the users, you can get cashback for purchases, find some of the best ongoing deals on the web and also take a look at reviews of products and services by our users, and you can also redeem great prizes!
                        </p>

                        <p>
                            Octaplus is also an e-commerce cashback platform for the benefit of retailers and consumers. Creating personalized shopping experiences with exclusive promotions and e-community features for consumers combined with high-level targeting and engagement for brands.We reach for the ultimate goal in retail – a shared experience, a recommendation to act on a like or a dislike. OctaPlus has been developed with both consumer and e–retailer in mind, bringing cash back retail shopping and personalized experiences to the consumer and invaluable, interpretive smart data to the retailer or merchant as well as increased traffic.
                        </p>

                        <p>
                            Get access to a whole range of exclusive offers. Our mission is to make shopping a better experience. Our vision is to be able to meet all your shopping needs. Be rewarded for being part of the Octaplus family through our Rewards. Share and discover the best ways to shop in our growing community in the Review.
                        </p>

                        <p class="h5">
                            What Is Cashback?
                        </p>

                        <p class="mb-1">
                            Earning Cashback as simple as 1-2-3
                        </p>

                        <p>
                            Sign up to Octaplus for FREE then browse &amp; activities cash back from your desired store. You will be redirected to the merchant site, just shop and pay like you usually do. Your cashback will automatically be credited to your Octaplus account up to 48 hours and you will be notified via email. The amount paid back (cashback) is a percentage of the amount the customer spent on an item. The cashback percentage varies from retailer to retailer, with some businesses offering higher cashback rates than others.
                        </p>

                        <p class="h5">
                            How to Get and Withdraw Your Cashback
                        </p>

                        <ol>
                            <li>
                                <p> Log in to your Octaplus account. </p>
                            </li>
                            <li>
                                <p> Look for your favorite online store on Octaplus and click on the store that you’d like to visit. A pop-up will appear and you will be re-directed to the store. Please do not close the tab or your cashback might not be tracked. </p>
                            </li>
                            <li>
                                <p> Shop and complete your order within the same tab so that Octaplus can track your purchase and the amount that you had spent. </p>
                            </li>
                            <li>
                                <p> After you complete a transaction and would like to make a new order, you will need to click through Octaplus again. </p>
                            </li>
                            <li>
                                <p> Repeat this entire process starting at Octaplus every time you make a purchase or you can check out the cashback tips in each store's description at the page bottom. </p>
                            </li>
                        </ol>

                        <p>
                            After you’ve accumulated a substantial amount of cashback in your Octaplus account, you may transfer the sum via Transfer to Bank and then withdraw as cash. Do note that the minimum available balance in your account must be RM10. Your withdrawal request will then be processed and deposited into the selected account between 7 and 14 working days.
                        </p>

                        <p class="h5 font-weight-bold">
                            Our Popular Stores
                        </p>

                        <p class="h5">
                            Zalora
                        </p>

                        <p>
                            ZALORA is the leading name in online fashion shopping, carrying an ever-expanding line of local and international brands tailored for consumers in the region. Our selection of over 50.000 products covers every aspect of fashion, from skirts to suits, sneakers to slip-ons, sportswear to watches, and so much more.Start your style journey by owning a well-rounded range of basics and off-duty essentials.
                        </p>

                        <p>
                            ZALORA shoppers can also shop according to the latest trends that are dominating the fashion runway, whether it’s a monochrome edit, athleisure styling, or this season’s highlights.
                        </p>

                        <p>
                            With the widest selection of fashion apparel, you can possibly find, and outstanding services like lightning-fast shipping, cash-on-delivery, and free returns, ZALORA brings you the best of Malaysia online shopping today!
                        </p>

                        <p class="h5">
                            Cotton On
                        </p>

                        <p>
                            Cotton On Group is Australia's largest global retailer, known for its fashion clothing and stationery brands. It has over 1,500 stores in 18 countries and employs 22,000 workers globally. It currently operates eight brands; Cotton On, Cotton On Body, Cotton On Kids, Rubi, Typo, Cotton On LOST, Factorie and Supré.
                        </p>

                        <p class="h5">
                            Nike
                        </p>

                        <p>
                            NIKE, Inc. is an American multinational corporation that is engaged in the design, development, manufacturing, and worldwide marketing and sales of footwear, apparel, equipment, accessories, and services.
                        </p>

                        <p class="h5">
                            Charles and Keith
                        </p>

                        <p>
                            CHARLES &amp; KEITH started with a vision of creating a line of innovative footwear with a clear design aesthetic for the sensible chic women. Prompted by the pursuit to be directional and innovative in the global market, the brand works closely with appointed business partners to develop at a sharp pace.
                        </p>

                        <p class="h5">
                            Adidas
                        </p>

                        <p>
                            Adidas designs for and with athletes of all kinds. Creators, who love to change the game. Challenge conventions. Break the rules and define new ones. Then break them again. We supply teams and individuals with athletic clothing pre-match. To stay focussed. We design sports apparel that get you to the finish line. To win the match.
                        </p>

                        <p class="h5">
                            Sephora
                        </p>

                        <p>
                            Sephora is a visionary beauty e-retailer in the region, run by a dedicated and experienced affiliate program team. Affiliate marketing has proven to be a key driver, due to Sephora's ever-increasing amount of classic and emerging brands across a broad range of product categories including skincare, color, fragrance, body, smile care, and haircare, in addition to Sephora's own private label.
                        </p>

                        <p class="h5">
                            Poplook
                        </p>

                        <p>
                            POPLOOK.com is the leading online shopping destination in Malaysia. Offering you the widest choice in baju kurung, muslimah dresses, maxi Dresses, jubah, tudung, handbags, jewellery. Free delivery for Malaysia and other selected destinations
                        </p>

                        <p>
                            Modesty, choice, quality, affordability, inclusion and social awareness are the values that underpin the POPLOOK label. With over 1,500 design options; sizes from XS to 4XL; and a seamless online/in-store shopping experience, Malaysia's homegrown Modest Fashion Label hopes to help all women live their best life through fashion. The fashion label carries clothing, headscarves, handbags and shoes as well as a children's range. Each year, the not-for-profit POPLOOK Gives Back campaign channels proceeds to charities that benefit women and children in need. Since its inception in 2009, the label has won numerous accolades, but the most important being the brand of choice to their customers seeking high quality modest fashion.
                        </p>

                        <p class="h5">
                            Pomelo
                        </p>

                        <p>
                            Launched in 2013, Pomelo is a modern fashion brand born in Asia with a global mindset: on-trend, online, on-the-go. With an undisputable sense of style at an unparalleled price, Pomelo aims to offer women everywhere their best look to become their best selves. Shipping is fast, free Min. RM 139 and all products are delivered with a 365 day return policy guaranteed.
                        </p>

                        <p>
                            POMELOFASHION.COM plays host to weekly New Arrivals incorporating basic, core fashion, and trend-led looks.With an ever-growing assortment, Pomelo's style range spans the realms of beachwear to sportswear, and everything in between.From its design studios in Bangkok, to the doorsteps of millions around the world, Pomelo now delivers to over 50 countries globally. Have you tried Pomelo?
                        </p>

                        <p class="h5">
                            Agoda
                        </p>

                        <p>
                            Agoda is one of the world’s fastest growing online travel booking platforms. From its beginnings as an e-commerce start-up based in Singapore in 2005, Agoda has grown to offer a global network of 2 million properties in more than 200 countries and territories worldwide.
                        </p>

                        <p class="h5">
                            Klook
                        </p>

                        <p>
                            Find discounted attraction tickets, tours with unique experiences, and more! Join local day tours to visit spectacular sights and go on delicious food trips around the city. Upon landing at the airport, we've got all kinds of transfers available for you. Discover and book amazing travel experiences with Klook!
                        </p>

                        <p class="h5">
                            KKday
                        </p>

                        <p>
                            KKday provides all kinds of unique experiences. Scuba diving, rock climbing, cooking classes, secret sights, full day tour, tickets, charter service and airport transfers, we’ve got all you want. Arrange your fantastic itinerary for yourself now!
                        </p>

                        <p class="h5">
                            Photobook
                        </p>

                        <p class="mb-1">
                            Photobook Malaysia offers the best personalisable prints such as canvas prints, cards, stationeries, calendars, prints and photo gifts. We believe your memories should be printed on more than just albums that you can share and admire wherever you are - like a tumbler for the on-the-go and a desk calendar for your office table. Don't forget to keep your home looking good with custom wall decor like metal prints and photo tiles, adding a contemporary style at the same time. Make your own gifts for any occasion including wedding, travel, baby milestone, birthday, graduation, special celebration such as Christmas and Valentine's Day. Curate your best photographs and create a custom gift today through a simple online designer that is programmed with useful tools for every personalisation need.
                        </p>

                        <p>
                            No more worries about finding the perfect gifts because nothing is more unique than personalised gifts from the heart.
                        </p>

                        <p class="h5">
                            Jeoel Jewellery
                        </p>

                        <p class="mb-1">
                            JEOEL is more than jewelry. It is not someone's dream to create just another business. It is not about making fast profits. It is about creating an attainable lifestyle.
                        </p>

                        <p>
                            Joy can be for everyone.
                        </p>

                        <p class="mb-1">
                            The young lady in her first job.The prudent entrepreneur.The hardworking wife and mother.The blushing young man looking for the perfect expression of love.
                        </p>

                        <p>
                            JEOEL looks like fine jewelry without the fine jewelry price tag. We believe everyone deserves to enjoy small luxuries, not just the privileged.
                        </p>

                        <p class="h5">
                            紫藤 Purple Cane
                        </p>

                        <p>
                            Purple Cane Holdings Sdn.Bhd. (298681 V) established in 1987, with innovative spirit integrated with ambitious and enterprising individuals, aiming at going through a cultural passage. According to Purple Cane founder Mr. Lim Hock Lam, the definition of cultural business establishment: is a human activity of the integration of soul, mind and practices. In a culture scene, it is a sublimation of cultural resources, promoting its productivity from low to high through one or more virtual processes or innovation that consequently and incessantly contribute to systematic and value cultural development in the human community. A cultural business establishment entrepreneur or organization are individual or institution involved in cultural activity as obligation and lifestyle.
                        </p>

                        <p class="h5">
                            Focus Point Online Store
                        </p>

                        <p>
                            Focus Point online store offers prescription glasses online at discount prices. Buy quality eyeglasses and contact lenses with us today.
                        </p>

                        <p>
                            Focus Point is officially recognized by the Malaysia Book of Records as the largest optical retail chain store in Malaysia and also the first and only optical retail chain store to be listed in Bursa Malaysia. With more than 180 outlets nationwide and more than 230 eye care professionals ready to serve you.
                        </p>

                        <p>
                            Customers have a wide range of fashionable eyewear to choose from at the concept stores such as Focus Point, Focus Point Signature, Focus Point Concept Store, Focus Point Outlet, Whoosh, Opulence, Eyefont, Solariz and i-Focus.
                        </p>

                        <p class="h5">
                            Youbeli
                        </p>

                        <p>
                            Youbeli.com is a premier multi-category online marketplace in Malaysia. Fully owned by Youbuy Online Sdn Bhd, Youbeli provides customers with an incredible shopping experience, providing everything at their fingertips, essentially being a one stop shopping destination.
                        </p>

                        <p>
                            <span class="h5 font-weight-normal"> Airasia fresh: </span>
                            <span> groceries for everyone </span>
                        </p>

                        <p>
                            Airasia fresh is a new eCommerce platform specialising in Fresh Produce, Frozen Food and Packaged Food. This unique marketplace is powered by Teleport, the logistics arm of airasia. Think of us as your neighbourhood grocery store, with best and freshest produce and food delivered to you the next day! We strive to provide our customers the best value and experience, including secure payment options and the best customer support.
                        </p>

                        <p class="h5">
                            JD Sport
                        </p>

                        <p>
                            JD Sports has everything you need to elevate your everyday casual look to eye-catching new heights. Shop the latest footwear from brands like Nike, adidas, Vans and Puma that deliver performance, style and comfort to fit your on-the-go lifestyle. If you’re looking for athletic slides, basketball sneakers, casual shoes, running gear and everything in between, JD Sports has you covered.
                        </p>

                        <p class="h5">
                            Watsons
                        </p>

                        <p>
                            One of the most well-known pharmaceutical companies in the country, Watson's is the one-stop destination for all your health and beauty products. Watsons Malaysia has a great selection of products for all your health and beauty needs.
                        </p>

                        <p class="h5">
                            Sport Directs
                        </p>

                        <p>
                            Your one stop sport shop for the biggest brands - browse trainers for Men, Women &amp; Kids. Plus sports fashion, clothing &amp; accessories. SportsDirect.com is a British sporting goods retailer, the primary retail asset of Sports Direct International plc. The company was formerly known as Sports Soccer and Sports World, but since 2007 branches of the chain have been re-branded as SportsDirect.com, the domain name of its online presence.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endsection