<div class="main-footer">
    <div class="footer-content pb-0">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-4 col-12  ">
                    <div class="footer-col mb-3">
                        <a href="{{ route('web') }}" class="router-link-exact-active open active"><img src="/images/logo2.png" class="mh-50 mt-2" style="height:50px"></a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-12  ">
                    <div class="footer-col">
                        <p class="footer-title p-0 m-0"><strong> About Octaplus </strong></p>
                        <ul class="p-1">
                            <li class="list-inline"><a href="/web/about" class="text-dark"> About Us </a></li>
                            <li class="list-inline"><a href="/web/terms" class="text-dark"> Terms of Service </a></li>
                            <li class="list-inline"><a href="/web/disclaimer" class="text-dark"> Disclaimer </a></li>
                            <li class="list-inline"><a href="/web/privacy" class="text-dark"> Privacy Policy </a></li>
                            <li class="list-inline"><a href="/web/cashback/terms" class="text-dark"> Cashback Terms </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-12  ">
                    <div class="footer-col">
                        <p class="footer-title p-0 m-0"><strong> More From Us </strong></p>
                        <ul class="p-1">
                            <li class="list-inline"><a href="/web/faq" class="text-dark"> FAQ </a></li>
                            <li class="list-inline"><a href="https://octaplus.io/user/octaplus/5ca471f969f12b24533b8c62/detail" class="text-dark"> Events </a></li>
                            <li class="list-inline"><a href="https://blog.octaplus.io" target="_blank" class="text-dark"> Blog </a></li>
                            <li class="list-inline"><a href="https://www.facebook.com/pg/octaplus.io/community" target="_blank" class="text-dark"> Customer Reviews </a></li>
                            <li class="list-inline"><a href="/web/career" class="text-dark"> Career </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-12  ">
                    <div class="row">
                        <div class=" col-xl-12 col-lg-12 col-md-12 col-12  ">
                            <div class="mobile-app-content">
                                <p class="footer-title no-margin pb-2"><strong> Mobile App Download </strong></p>
                                <div class="row ">
                                    <div class="col-6 ">
                                        <a target="_blank" href="https://itunes.apple.com/app/octaplus/id1457072183" class="app-icon">
                                            <img src="/images/web/site/app_store_badge.svg" alt="Octaplus available on the App Store" title="Octaplus available on the App Store"></a>
                                    </div>
                                    <div class="col-6 ">
                                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.alcodes.octaplus" class="app-icon">
                                            <img src="/images/web/site/google-play-badge.svg" alt="Octaplus available on the Play Store" title="Octaplus available on the Play Store"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" col-xl-12 col-lg-12 col-md-12 col-12  ">
                            <div class="mobile-app-content">
                                <p class="footer-title no-margin"><strong> Follow Us </strong></p>
                                <div>
                                    <div class="sharethis-inline-follow-buttons st-inline-follow-buttons st-#{action_pos}  st-animated" id="st-1">
                                        <div class="st-btn st-first rounded" data-network="facebook" style="display: inline-block;">
                                            <a href="https://www.facebook.com/octaplus.io"><img alt="facebook sharing button" src="/images/icon/facebook.svg"></a>
                                        </div>
                                        <div class="st-btn rounded" data-network="twitter" style="display: inline-block;">
                                            <a href="https://www.twitter.com/octaplus_my"><img alt="twitter sharing button" src="/images/icon/twitter.svg"></a>
                                        </div>
                                        <div class="st-btn rounded" data-network="instagram" style="display: inline-block;">
                                            <a href="https://www.instagram.com/octaplus_my/"><img alt="instagram sharing button" src="/images/icon/instagram.svg"></a>
                                        </div>
                                        <div class="st-btn rounded" data-network="youtube" style="display: inline-block;">
                                            <a href="https://www.youtube.com/channel/UCrRu6yb7igjYRMncIggNOVA?sub_confirmation=1"><img alt="youtube sharing button" src="/images/icon/youtube.svg"></a>
                                        </div>
                                        <div class="st-btn st-last rounded" data-network="pinterest" style="display: inline-block;">
                                            <a href="https://www.pinterest.com/octaplus_my/"><img alt="pinterest sharing button" src="/images/icon/pinterest.svg"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-12 bg-orange p-2">
            <div class="container text-white">
                <div class="copy-info text-center">
                    © 2019 Octaplus - Buy . Earn . Explore | All Rights Reserved.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="finds-footer" id="finds-footer" style="height: 60px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex align-items-center justify-content-center">
                    <img src="/images/icon/sign-up.svg" class="my-1 mr-3">
                    <p class="mb-0 mr-3"><span>Sign Up &amp; Get Your RM3 Reward!</span></p>
                    <a class="btn bg-orange text-white px-auto px-sm-4" data-toggle="modal" data-target="#loginModal"> Sign Up Now </a>
                </div>
            </div>
        </div>
    </div>
</div>