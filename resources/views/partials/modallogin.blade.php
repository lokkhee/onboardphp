<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loginModal">Welcome</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id='login-modal' style="display:block">
                @include('auth.login')
            </div>
            <div class="modal-body" id='register-modal' style="display:none">
                @include('auth.register')
            </div>
        </div>
    </div>
</div>

<script>
    function loginOrRegister() {
        var x = document.getElementById("login-modal");
        var y = document.getElementById("register-modal");
        if (x.style.display === "block") {
            x.style.display = "none";
            y.style.display = "block";
            history.pushState('', 'Register', '/web/register')
        } else {
            x.style.display = "block";
            y.style.display = "none";
            history.pushState('', 'Register', '/web/login')
        }
    }
</script>

@section('scripts')
@parent

@if($errors->has('email') || $errors->has('password'))
<script>
    $(function() {
        $('#loginModal').modal({
            show: true
        });
    });
</script>
@endif
@endsection